# OracleDBイメージ作成とコンテナ利用手順

## OracleDBイメージ作成

```bash
$ git clone https://github.com/oracle/docker-images
Cloning into 'docker-images'...
remote: Enumerating objects: 17365, done.
remote: Counting objects: 100% (1942/1942), done.
remote: Compressing objects: 100% (252/252), done.
remote: Total 17365 (delta 1760), reused 1722 (delta 1679), pack-reused 15423
Receiving objects: 100% (17365/17365), 15.01 MiB | 10.71 MiB/s, done.
Resolving deltas: 100% (10283/10283), done.
```

```bash
$ ll
total 8
-rw-r--r--   1 user  staff   520B  1  9 22:19 README.md
drwxr-xr-x  44 user  staff   1.4K  1  9 21:26 docker-images
```

```bash
cd docker-images/OracleDatabase
$ ll
total 8
drwxr-xr-x   7 user  staff   224B  1  9 21:25 RAC
-rw-r--r--   1 user  staff   878B  1  9 21:25 README.md
drwxr-xr-x  11 user  staff   352B  1  9 21:25 SingleInstance
$ cp -p ~/Downloads/LINUX.X64_193000_db_home.zip SingleInstance/dockerfiles/19.3.0/.
```

ビルド実行

```bash
cd SingleInstance/dockerfiles
sh buildContainerImage.sh -v 19.3.0 -e -i
```

イメージ確認

```bash
$ docker images
REPOSITORY        TAG         IMAGE ID       CREATED         SIZE
oracle/database   19.3.0-ee   839029700504   2 minutes ago   6.54GB
```

イメージファイル作成

```bash
docker save oracle/database:19.3.0-ee | gzip > oracledatabase-19.3.0-ee.tar.gz
```

## OracleDBイメージロード

```bash
$ docker load -i oracledatabase-19.3.0-ee.tar.gz 
6a9ddae085fc: Loading layer [==================================================>]  3.584kB/3.584kB
Loaded image: oracle/database:19.3.0-ee
```

イメージ確認

```bash
$ docker images
REPOSITORY        TAG         IMAGE ID       CREATED       SIZE
oracle/database   19.3.0-ee   839029700504   3 hours ago   6.54GB
```

コンテナ起動

```bash
docker compose up -d
```
